#!/bin/bash

install () {
    mkdir $HOME/.local/share/fonts

    wget https://gitlab.com/yakei/desktop/-/raw/main/fonts/MaterialIcons-Regular.ttf -P $HOME/.local/share/fonts 

    sudo emerge -a papirus-icon-theme rofi bluez xorg setxkbmap xfce4-power-manager playerctl sddm alacritty nemo nemo-fileroller grim slurp wl-clipboard polkit-gnome hyprland

    sudo mkdir -p /etc/portage/patches/gui-apps/waybar
    sudo mv patches/hyprctl.patch /etc/portage/patches/gui-apps/waybar/
    su -c "echo 'gui-apps/waybar experimental network pipewire wifi tray' >> /etc/portage/package.use/waybar"
    su -c "echo 'gui-apps/waybar ~amd64' >> /etc/portage/package.accept_keywords/waybar"

    sudo emerge -a dev-vcs/git eselect-repository
    sudo eselect repository enable guru
    sudo emerge --sync guru
    su -c "echo 'gui-apps/swaync ~amd64' >> /etc/portage/package.accept_keywords/swaync" # as root, sudo won't work.
    sudo emerge -a swaync

    su -c "echo 'app-misc/brightnessctl ~amd64' >> /etc/portage/package.accept_keywords/brightnessctl" # as root, sudo won't work.
    sudo emerge -a brightnessctl


    [ ! -d ~/.config/hypr ] 	&& cp -r hypr ~/.config/. || cp -r ~/.config/hypr/ ~/.config/.hypr-$USER && rm -rf ~/.config/hypr/ && cp -r hypr ~/.config/.
    [ ! -f ~/.config/waybar ] && cp -r waybar ~/.config/. || cp -r  ~/.config/waybar ~/.config/.waybar-$USER && cp -r waybar ~/.config/.
    [ ! -d ~/.config/rofi ] 	&& cp -r rofi ~/.config/.    || cp -r ~/.config/rofi ~/.config/.rofi-backup && rm -rf ~/.config/rofi && cp -r rofi ~/.config/.
    [ ! -d ~/.config/alacritty ] && cp -r alacritty ~/.config/.   || cp -r ~/.config/alacritty ~/.config/.alacritty-$USER && rm -rf ~/.config/alacritty && cp -r alacritty ~/.config/.
    [ ! -d ~/.config/swaync ] && cp -r swaync ~/.config/.   || cp -r ~/.config/swaync ~/.config/.swaync-$USER && rm -rf ~/.config/swaync && cp -r swaync ~/.config/.

    echo " Log out and select Hyprland from your login manager. "
}

start () {
    echo "Would you like to being the install?"
    read -p "[yes/no]: " option

    if [ $option = "yes" ]
    then
        install
    else
        echo "Aborted."
    fi
}

