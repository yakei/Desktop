#!/usr/bin/env bash

dir="$HOME/.config/rofi/launchers/type-1"
theme='style-5'

rofi \
        -show p \
        -modi p:'rofi-power-menu --symbols-font "IosevkaTerm NerdFont Mono 17"' \
        -font "JetBrainsMono 16" \
        -theme ${dir}/${theme}.rasi
