#!/bin/bash

install () {
    yay -S hyprland waybar-hyprland-git 

    mkdir $HOME/.local/share/fonts

    wget https://gitlab.com/yakei/desktop/-/raw/main/fonts/MaterialIcons-Regular.ttf -P $HOME/.local/share/fonts 

    yay -S papirus-icon-theme rofi bluez xorg-setxkbmap xfce4-power-manager playerctl sddm alacritty nemo nemo-fileroller grim slurp wl-clipboard polkit-gnome swaync hyprland waybar-hyprland-git

    [ ! -d ~/.config/hypr ] 	&& cp -r hypr ~/.config/. || cp -r ~/.config/hypr/ ~/.config/.hypr-$USER && rm -rf ~/.config/hypr/ && cp -r hypr ~/.config/.
    [ ! -f ~/.config/waybar ] && cp -r waybar ~/.config/. || cp -r  ~/.config/waybar ~/.config/.waybar-$USER && cp -r waybar ~/.config/.
    [ ! -d ~/.config/rofi ] 	&& cp -r rofi ~/.config/.    || cp -r ~/.config/rofi ~/.config/.rofi-backup && rm -rf ~/.config/rofi && cp -r rofi ~/.config/.
    [ ! -d ~/.config/alacritty ] && cp -r alacritty ~/.config/.   || cp -r ~/.config/alacritty ~/.config/.alacritty-$USER && rm -rf ~/.config/alacritty && cp -r alacritty ~/.config/.
    [ ! -d ~/.config/swaync ] && cp -r swaync ~/.config/.   || cp -r ~/.config/swaync ~/.config/.swaync-$USER && rm -rf ~/.config/swaync && cp -r swaync ~/.config/.

    echo " Log out and select Hyprland from your login manager. "
}


start () {
    echo "Would you like to being the install?"
    read -p "[yes/no]: " option

    if [ $option = "yes" ]
    then
        install
    else
        echo "Aborted."
    fi
}



