<div align="center">
    <h1>Yakei Desktop</h1>
</div>

---

![Desktop](./.assets/desktop.png)

---


<div align="center">
    <h1>Welcome to Yakei Desktop</h1>
</div>
<div align="center">

Yakei Desktop is based on Hyprland, It uses waybar for it's bar and a lot of other utilities.

</div>


## Information

---
<img align="right" width="550px" src="./.assets/neofetch.png"/>

**My setup**:

- **OS**: [Gentoo](https://gentoo.org)
- **WM**: [Hyprland](https://github.com/hyprwm/Hyprland)
- **Terminal**: [Alacritty](https://github.com/alacritty/alacritty)
- **Shell**: [Fish](https://github.com/fish-shell/fish-shell)
- **Application Launcher**: [Rofi](https://github.com/davatorium/rofi)
- **File Manager**: [Nemo](https://github.com/linuxmint/nemo)
- **Icons**: [Papirus-Dark](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
- **Cursor**: [BreezeX-light](https://github.com/ful1e5/BreezeX_Cursor)
- **Font**: [IosevkaTermNerdFont](https://www.nerdfonts.com/font-downloads)
- **Editor**: [VS Code](https://code.visualstudio.com/),[Helix](https://helix-editor.com/)


## Installation

---

#### Here are three levels of what you need

1. Dependencies you need to have in order for the rice to work.
2. Important dependencies but can be replaced, which may cause some issues.
3. Nice to have and are the default. Can be easily changed without any worries!

# If using the install script, you should set a password for the root user.

#### Using the install script (EXPERIMENTAL)

> **Arch**
```bash
./install-arch.sh
```
> **Gentoo**
```bash
./install-gentoo.sh
```

<details>
<summary><b>1. Dependencies</b></summary>

#### **Hyprland**

>**Arch**

```bash
yay -S hyprland # Or hyprland-nvidia if you have a nvidia gpu.
```
> **Gentoo**

```bash
sudo emerge -a hyprland xdg-desktop-portal-hyprland
```

> **Fedora**
```bash
sudo dnf copr enable solopasha/hyprland
sudo dnf in hyprland # or hyprland-nvidia if you have a nvidia card
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in hyprland
```

> **Void**
```bash
# Follow https://github.com/Makrennel/hyprland-void for instructions
```

### **Waybar**

>**Arch**

```bash
yay -S waybar-hyprland-git
```
> **Gentoo**

```bash
sudo mkdir -p /etc/portage/patches/gui-apps/waybar
sudo mv patches/hyprctl.patch /etc/portage/patches/gui-apps/waybar/
echo "gui-apps/waybar experimental network pipewire wifi tray" >> /etc/portage/package.use/waybar
echo "gui-apps/waybar ~amd64" >> /etc/portage/package.accept_keywords/waybar
```

> **Fedora**
```bash
sudo dnf copr enable solopasha/hyprland
sudo dnf in waybar-hyprland 
```

> **openSUSE Tumbleweed**
```bash
# Enable openSUSE source repos
sudo zypper mr -e repo-source
sudo zypper ref
sudo zypper si waybar
mkdir ../Build
git clone https://github.com/Alexays/Waybar ../Build/
cd ../Build/Waybar
git apply ../desktop/patches/hyprctl.patch
meson setup --buildtype=plain --prefix=/usr -Dsndio=disabled -Dcava=disabled -Dexperimental=true build
sudo ninja -C build install
```

> **Void**
```bash
sudo xbps-install -S ibevdev-devel libinput-devel wayland-devel gtkmm-devel spdlog eudev-libudev-devel gtk-layer-shell-devel jsoncpp-devel libglib-devel libsigc++-devel fmt-devel chrono-date-devel playerctl-devel pulseaudio-devel wireplumber-devel pipewire-devel ninja base-devel meson git cmake pkg-config glib-devel wayland-devel scdoc gobject-introspection 
git clone https://github.com/Alexays/Waybar ../Build/
cd ../Build/Waybar
git apply ../desktop/patches/hyprctl.patch
meson setup --buildtype=plain --prefix=/usr -Dsndio=disabled -Dcava=disabled -Dexperimental=true -Dsystemd=disabled -Dgtk-layer-shell=enabled -Dlibudev=enabled build
sudo ninja -C build install

```

#### **Rofi**

>**Arch**

```bash
sudo pacman -S rofi
```
> **Gentoo**

```bash
sudo emerge -a rofi
```

> **Fedora**
```bash
sudo dnf in rofi
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in rofi
```

> **Void**
```bash
sudo xbps-install -S rofi
```

</details>
<details>

<br>

<summary><b>2. Important Dependencies</b></summary>

#### **Papirus** - Icon Theme

***Make sure the theme is located at `/usr/share/icons/`***

> **Arch**

```bash
sudo pacman -S papirus-icon-theme
```
> **Gentoo**

```bash
sudo emerge -a papirus-icon-theme
```

> **Fedora**
```bash
sudo dnf in papirus-icon-theme
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in papirus-icon-theme
```

> **Void**
```bash
sudo xbps-install -S papirus-icon-theme
```

### **Swaync - Notification Daemon**

> **Arch**
```bash
sudo pacman -S swaync
```
> **Gentoo**

```bash
sudo emerge -a dev-vcs/git eselect-repository
sudo eselect repository enable guru
sudo emerge --sync guru
echo "gui-apps/swaync ~amd64" >> /etc/portage/package.accept_keywords/swaync # as root, sudo won't work.
sudo emerge -a swaync
```

> **Fedora**
```bash
sudo dnf copr enable erikreider/SwayNotificationCenter
sudo dnf in SwayNotificationCenter
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in SwayNotificationCenter
```

> **Void**
```bash
sudo xbps-install -S SwayNotificationCenter
```

### **Polkit-gnome - Polkit**
```bash
sudo pacman -S polkit-gnome
```
> **Gentoo**

```bash
sudo emerge -a polkit-gnome
```

> **Fedora**
```bash
sudo dnf in polkit-gnome
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in polkit-gnome
```

> **Void**
```bash
sudo xbps-install -S polkit-gnome
```

#### **Brightnessctl - Brightness control**

> **Arch**

```bash
sudo pacman -S brightnessctl
```
> **Gentoo**

```bash
sudo emerge -a dev-vcs/git eselect-repository
sudo eselect repository enable guru
sudo emerge --sync guru
echo "app-misc/brightnessctl ~amd64" >> /etc/portage/package.accept_keywords/brightnessctl # as root, sudo won't work.
sudo emerge -a brightnessctl
```

> **Fedora**
```bash
sudo dnf in brightnessctl
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in brightnessctl
```

> **Void**
```bash
sudo xbps-install -S brightnessctl
```

#### **bluez/bluetoothctl - Bluetooth**

> **Arch**

```bash
sudo pacman -S bluez
```
> **Gentoo**

```bash
sudo emerge -a bluez
```

> **Fedora**
```bash
sudo dnf in bluez
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in bluez
```

> **Void**
```bash
sudo xbps-install -S bluez
```

#### **setxkbmap - Change Keyboard Layout**

> **Arch**

```bash
sudo pacman -S xorg-setxkbmap
```
> **Gentoo**

```bash
sudo emerge -a setxkbmap
```

> **Fedora**
```bash
sudo dnf in setxkbmap
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in setxkbmap
```

> **Void**
```bash
sudo xbps-install -S setxkbmap
```

#### **xfce4-power-manager - Power Manager**

> **Arch**

```bash
sudo pacman -S xfce4-power-manager
```
> **Gentoo**

```bash
sudo emerge -a xfce4-power-manager
```

> **Fedora**
```bash
sudo dnf in xfce4-power-manager
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in xfce4-power-manager
```

> **Void**
```bash
sudo xbps-install -S xfce4-power-manager
```

#### **playerctl - Media control**

> **Arch**

```bash
sudo pacman -S playerctl
```
> **Gentoo**

```bash
sudo emerge -a playerctl
```

> **Fedora**
```bash
sudo dnf in playerctl
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in playerctl
```

> **Void**
```bash
sudo xbps-install -S playerctl
```

#### **SDDM - Login/Logout**

> **Arch**

```bash
sudo pacman -S sddm
```
> **Gentoo**

```bash
sudo emerge -a sddm
```

> **Fedora**
```bash
sudo dnf in sddm
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in sddm
```

> **Void**
```bash
sudo xbps-install -S sddm
```

</details>

<details>
<summary><b>3. Optional Dependencies</b></summary>

#### **Alacritty** - Default terminal

> **Arch**

```bash
sudo pacman -S alacritty
```
> **Gentoo**

```bash
sudo emerge -a alacritty
```

> **Fedora**
```bash
sudo dnf in alacritty
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in alacritty
```

> **Void**
```bash
sudo xbps-install -S alacritty
```

#### **Nemo** - Default file manager

> **Arch**

```bash
sudo pacman -S nemo nemo-fileroller
```
> **Gentoo**

```bash
sudo emerge -a nemo nemo-fileroller
```

> **Fedora**
```bash
sudo dnf in nemo nemo-fileroller
```

> **openSUSE Tumbleweed**
```bash
sudo zypper in nemo nemo-extension-fileroller
```

> **Void**
```bash
sudo xbps-install -S nemo nemo-fileroller
```

#### **grim, slurp and wl-clipboard - Screenshots**

> **Arch**

```bash
sudo pacman -S grim slurp wl-clipboard
```

</details>

#### Setup

Make sure to backup any data before you copy my deskop over!

```bash
git clone https://gitlab.com/Yakei/desktop
cd desktop
cp -r configs/hypr ~/.config/
cp -r configs/waybar ~/.config/
cp -r configs/rofi ~/.config/
cp -r alacritty ~/.config/
```

### PS: After installing my dotfiles you need to change the user in $HOME/.config/swaync/config.json from alxhr to yours.

